#pragma once
#include <ftl/test.hpp>
#include <cstdint>
#include <cmath>
#include <numeric>
#include <algorithm>
#include <initializer_list>

namespace ftl {
    template <class T, int N>
    struct vector_t {
        T data[N];

        vector_t(std::initializer_list<T> init_vector) : data(init_vector) {}

        auto x() const -> T { static_assert(N >= 1, "Wrong vector size"); return data[0]; }
        auto y() const -> T { static_assert(N >= 2, "Wrong vector size"); return data[1]; }
        auto z() const -> T { static_assert(N >= 3, "Wrong vector size"); return data[2]; }
        auto w() const -> T { static_assert(N >= 4, "Wrong vector size"); return data[3]; }

        void normalize() {
            auto square_sum = std::accumulate(cbegin(data), cend(data), 0, [](const T lhs, const T rhs) { return lhs + rhs * rhs; });
            auto sum_sqared = ::sqrt(square_sum);
            std::transform(begin(data), end(data), begin(data), [=](const T value) { return value / square_sum; });
        }
    };

    struct v2 : public vector_t<float, 2> {};
    struct v3 : public vector_t<float, 3> {};
    struct v4 : public vector_t<float, 4> {};
}
