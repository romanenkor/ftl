#pragma once
#include <ftl/types.hpp>
#include <ftl/meta.hpp>
#include <ftl/test.hpp>
#include <iterator>
#include <utility>
#include <tuple>
#include <type_traits>
#include <utility>

namespace ftl {
    template <class node_t>
    void link(node_t* new_node, node_t* old_node) {
        new_node->m_next = old_node;
        new_node->m_prev = old_node->m_prev;
        old_node->m_prev->m_next = new_node;
        old_node->m_prev = new_node;
    }

    template <class node_t>
    void unlink(node_t* node) {
        node->m_prev->m_next = node->m_next;
        node->m_next->m_prev = node->m_prev;
    }

    namespace detail {
        template <class derived_t>
        class mixin {
        protected:
            auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
        };
    }

    namespace detail {
        template <class value_t>
        using erase_type = decltype(std::declval<value_t>().erase(std::declval<value_t>().cbegin()));

        template <class value_t>
        using reverse_type = decltype(std::declval<value_t>().rbegin());

        template <class derived_t>
        class insert_mixin {
        public:
            template <class iterator_t, class value_t>
            auto insert(iterator_t pos, const value_t& value) {
                return this->derived().template emplace<value_t>(pos, value);
            }

            template <class iterator_t, class value_t>
            auto insert(iterator_t pos, value_t&& value) {
                return this->derived().template emplace<value_t>(pos, value);
            }

            template <class value_t>
            auto push_front(const value_t& value) {
                return this->derived().template insert<value_t>(this->derived().cbegin(), value);
            }
            
            template <class value_t>
            auto push_front(value_t&& value) {
                return this->derived().template insert<value_t>(this->derived().cbegin(), value);
            }

            template <class value_t, class... args_t>
            auto emplace_front(args_t&&... args) {
                return this->derived().template emplace<value_t>(this->derived().cbegin(), std::advance<args_t>(args)...);
            }

            template <class value_t>
            auto push_back(const value_t& value) {
                return this->derived().template insert<value_t>(this->derived().cend(), value);
            }
            
            template <class value_t>
            auto push_back(value_t&& value) {
                return this->derived().template insert<value_t>(this->derived().cend(), value);
            }

            template <class value_t, class... args_t>
            auto emplace_back(args_t&&... args) {
                return this->derived().template emplace<value_t>(this->derived().cend(), std::forward<args_t>(args)...);
            }
        private:
            auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
        };

        template <class derived_t, bool enabled>
        class erase_mixin {
        public:
            inline void pop_front() {
                this->derived().erase(this->derived().cbegin());
            }

            inline void pop_back() {
                this->derived().erase(--this->derived().cend());
            }
        private:
            auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
        };

        template <class derived_t>
        class erase_mixin<derived_t, false> {};

        template <class derived_t>
        class advance_mixin {
        public:
            auto cbegin() const {
                return this->derived().begin();
            }

            auto cend() const {
                return derived().end();
            }
        private:
            auto derived() const -> const derived_t& { return *static_cast<const derived_t*>(this); } 
        };

        template <class derived_t, bool enabled>
        class reverse_mixin {
        public:
            auto crbegin() const {
                return this->derived().rbegin();
            }

            auto crend() const {
                return this->derived().rend();
            }
        private:
            auto derived() const -> const derived_t& { return *static_cast<const derived_t*>(this); } 
        };

        template <class derived_t>
        struct reverse_mixin<derived_t, false> {};
    }

    template <class derived_t>
    struct container :
        public detail::insert_mixin<derived_t>,
        public detail::advance_mixin<derived_t>,
        public detail::reverse_mixin<derived_t, is_detected_v<detail::reverse_type, derived_t>>,
        public detail::erase_mixin<derived_t, is_detected_v<detail::erase_type, derived_t>>
    {};

    template <class index_seq_t, template <class> class... elements_t>
    class composition_impl_t;

    template <class derived_t, template <class> class element_t, unsigned index, unsigned count>
    struct mixin_element_t : element_t<mixin_element_t<derived_t, element_t, index, count>> {};

    template <unsigned i, unsigned index, unsigned count, class derived_t, template <class> class element_t, template <class> class... elements_t>
    struct get_template_impl_t {
        using result_t = typename get_template_impl_t<i + 1, index, count, derived_t, elements_t...>::result_t;
    };

    template <unsigned index, unsigned count, class derived_t, template <class> class element_t, template <class> class... elements_t>
    struct get_template_impl_t<index, index, count, derived_t, element_t, elements_t...> {
        using result_t = mixin_element_t<derived_t, element_t, index, count>;
    };

    template <unsigned index, unsigned count, class derived_t>
    struct get_template_t;

    template <unsigned index, unsigned count, unsigned... indices, template <class> class... element_t>
    struct get_template_t<index, count, composition_impl_t<index_sequence_t<indices...>, element_t...>> {
        using derived_t = composition_impl_t<index_sequence_t<indices...>, element_t...>;
        using result_t = typename get_template_impl_t<0, index, count, derived_t, element_t...>::result_t;
    };

    template <class derived_t, unsigned count>
    struct implementation_details_t {
        template <unsigned i, class implementation_t, class interface_t, class lambda_t>
        static void next_check_interface(implementation_t* _this, interface_t interface, lambda_t&& callback, std::true_type) {
            auto base = static_cast<derived_t*>(_this);
            auto member = static_cast<typename get_template_t<i, count, derived_t>::result_t*>(base);
            next_call_or_continue<i>(interface, member, callback, std::integral_constant<bool, std::is_base_of<interface_t, typename std::remove_reference<decltype(*member)>::type>::value>{});
        }

        template <unsigned i, class implementation_t, class interface_t, class lambda_t>
        static void next_check_interface(implementation_t* /* _this */, interface_t /* interface */, lambda_t&& /* callback */, std::false_type) {
            static_assert(i < count, "Unable to find implementation conforming to desired interface");
        }

        template <unsigned i, class implementation_t, class interface_t, class wrapper_t, class lambda_t>
        static void next_call_or_continue(implementation_t* /* _this */, interface_t /* interface */, wrapper_t member, lambda_t&& callback, std::true_type) {
            return callback(*member);
        }
        
        template <unsigned i, class implementation_t, class interface_t, class wrapper_t, class lambda_t>
        static void next_call_or_continue(implementation_t* _this, interface_t interface, wrapper_t /* member */, lambda_t&& callback, std::false_type) {
            return next_check_interface<i + 1>(_this, interface, std::forward<lambda_t>(callback), std::integral_constant<bool, i + 1 < count>{});
        }

        template <unsigned i, class implementation_t, class interface_t, class lambda_t>
        static void for_each_check_interface(implementation_t* _this, interface_t interface, lambda_t&& callback, std::true_type) {
            auto base = static_cast<derived_t*>(_this);
            auto member = static_cast<typename get_template_t<i, count, derived_t>::result_t*>(base);
            for_each_call_and_continue<i>(_this, interface, member, callback, std::integral_constant<bool, std::is_base_of<interface_t, typename std::remove_reference<decltype(*member)>::type>::value>{});
        }

        template <unsigned i, class implementation_t, class interface_t, class lambda_t>
        static void for_each_check_interface(implementation_t* /* _this */, interface_t /* interface */, lambda_t&& /* callback */, std::false_type) {}

        template <unsigned i, class implementation_t, class interface_t, class wrapper_t, class lambda_t>
        static void for_each_call_and_continue(implementation_t* _this, interface_t interface, wrapper_t member, lambda_t&& callback, std::true_type) {
            callback(*member);
            return for_each_check_interface<i + 1>(_this, interface, std::forward<lambda_t>(callback), std::integral_constant<bool, i + 1 < count>{});
        }
        
        template <unsigned i, class implementation_t, class interface_t, class wrapper_t, class lambda_t>
        static void for_each_call_and_continue(implementation_t* _this, interface_t interface, wrapper_t /* member */, lambda_t&& callback, std::false_type) {
            return for_each_check_interface<i + 1>(_this, interface, std::forward<lambda_t>(callback), std::integral_constant<bool, i + 1 < count>{});
        }
    };

    template <unsigned... index, template <class> class... element_t>
    class composition_impl_t<index_sequence_t<index...>, element_t...> :  mixin_element_t<composition_impl_t<index_sequence_t<index...>, element_t...>, element_t, index, sizeof...(element_t)>... {
        friend implementation_details_t<composition_impl_t<index_sequence_t<index...>, element_t...>, sizeof...(element_t)>;
        using this_t = composition_impl_t<index_sequence_t<index...>, element_t...>;
    public:
        template <class interface_t, class lambda_t>
        void next(interface_t interface, lambda_t&& callback) {
            return impl.template next_check_interface<0>(this, interface, callback, std::integral_constant<bool, 0 < sizeof...(element_t)>{});
        }

        template <class interface_t, class lambda_t>
        void for_each(interface_t interface, lambda_t&& callback) {
            return impl.template for_each_check_interface<0>(this, interface, callback, std::integral_constant<bool, 0 < sizeof...(element_t)>{});
        }
    private:
        implementation_details_t<composition_impl_t<index_sequence_t<index...>, element_t...>, sizeof...(element_t)> impl;
    };

    template <template <class> class... element_t>
    struct composition_t : composition_impl_t<typename make_index_sequence_t<sizeof...(element_t)>::type, element_t...> {};

    template <class derived_t>
    class implementation_t;

    template <class derived_t, template <class> class element_t, unsigned index, unsigned count>
    class implementation_t<mixin_element_t<derived_t, element_t, index, count>> {
        friend element_t<mixin_element_t<derived_t, element_t, index, count>>;
    private:
        template <class interface_t, class lambda_t>
        void next(interface_t interface, lambda_t&& callback) {
            return impl.template next_check_interface<index + 1>(this, interface, callback, std::integral_constant<bool, index + 1 < count>{});
        }

        template <class interface_t, class lambda_t>
        void for_each(interface_t interface, lambda_t&& callback) {
            return impl.template for_each_check_interface<0>(this, interface, callback, std::integral_constant<bool, 0 < count>{});
        }
    private:
        implementation_details_t<derived_t, count> impl;
    };
}

#ifdef ENABLE_UNIT_TESTS
struct test_iface_t {};

template <class derived_t>
struct test_impl_t : test_iface_t {
    bool test() {
        return true;
    }
};

bool test_mixins() {
    using namespace ftl;
    composition_t<test_impl_t, test_impl_t> a{};
    bool result = false;
    a.for_each(test_iface_t{}, [&](auto& object) {
        result |= object.test();
    });
    return result;
}

TEST
{
    REQUIRE(test_mixins());
}
#endif
