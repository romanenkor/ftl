#pragma once
#include <ftl/types.hpp>
#include <cstdint>

namespace ftl {
    namespace detail {
        struct type_info_table_t {
            std::size_t counter;
        };

        template <class value_t>
        struct type_info_t {
            static std::size_t id;
        };

        template <class value_t>
        std::size_t type_info_t<value_t>::id = []{
            return static_<type_info_table_t>::get().counter++;
        }();
    }

    template <class value_t>
    auto type_id() -> std::size_t {
        return detail::type_info_t<value_t>::id;
    }
}
