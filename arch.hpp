#pragma once
#if defined(__i386__) || defined(__i386) || defined(_M_IX86) || defined(__X86__) || defined(_X86_) || defined(__386)
#   include "detail/arch/x86.hpp"
#endif
 
#if defined(__x86_64__) || defined(_M_AMD64)
#   include "detail/arch/x86_64.hpp"
#endif

#include <ftl/test.hpp>

TEST
{
    // BROKEN
}
