#pragma once
#include <ftl/meta.hpp>
#include <utility>
#include <type_traits>

namespace ftl {
    namespace detail {
        template <class value_t, class... args_t>
        using backward_type = decltype(--std::declval<value_t>());

        template <class value_t>
        const constexpr bool is_backward_v = is_detected_t<backward_type, value_t>::value;

        template <class derived_t, bool enabled>
        struct backward_range {
            auto operator-=(std::ptrdiff_t n) -> derived_t& {
                while(n > 0) { --this->derived(); --n; }
                return this->derived();
            }

            friend auto operator-(const derived_t& lhs, std::ptrdiff_t n) -> derived_t {
                auto copy = lhs;
                while(n > 0) { --copy; --n; }
                return copy;
            }
        private:
            auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
        };

        template <class derived_t>
        struct backward_range<derived_t, false> {};
    }

    template <class derived_t>
    struct backward_range {
        auto operator--(int) -> derived_t {
            auto copy = this->derived();
            return --this->derived(), copy;
        }
    private:
        auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
    };

    namespace detail {
        template <class value_t, class... args_t>
        using comparable_type = decltype(std::declval<value_t>() += 1);

        template <class value_t>
        const constexpr bool is_comparable_v = is_detected_t<comparable_type, value_t>::value;


        template <class derived_t, bool enabled>
        struct comparable_range {
            friend auto operator<(const derived_t&, const derived_t&) {
                return false;
            }

            friend auto operator>(const derived_t&, const derived_t&) {
                return false;
            }

            friend auto operator<=(const derived_t&, const derived_t&) {
                return false;
            }

            friend auto operator>=(const derived_t&, const derived_t&) {
                return false;
            }
        private:
            auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
        };

        template <class derived_t>
        struct comparable_range<derived_t, false> {};
    }

    namespace detail {
        template <class value_t, class... args_t>
        using equal_type = decltype(std::declval<value_t>() == std::declval<value_t>());

        template <class value_t>
        const constexpr bool is_equal_v = is_detected_t<equal_type, value_t>::value;

        template <class derived_t, bool enabled>
        struct equal_range {
            friend auto operator!=(const derived_t& lhs, const derived_t& rhs) {
                return !(lhs == rhs);
            }
        };

        template <class derived_t>
        struct equal_range<derived_t, false> {};
    }

    namespace detail {
        template <class value_t, class... args_t>
        using has_prefix_inc_t = decltype(++std::declval<value_t>());

        template <class value_t, class... args_t>
        using has_postfix_inc_t = decltype(std::declval<value_t>()++);

        template <class value_t>
        constexpr bool has_prefix_inc_v = is_detected_v<has_prefix_inc_t, value_t>;

        template <class value_t>
        constexpr bool has_postfix_inc_v = is_detected_v<has_prefix_inc_t, value_t>;

        template <class value_t>
        constexpr bool is_forward_v = is_detected_v<has_prefix_inc_t, value_t>
                                    | is_detected_v<has_postfix_inc_t, value_t>;

        template <class derived_t, bool enabled>
        struct forward_range {
            auto operator+=(std::ptrdiff_t n) -> derived_t& {
                while(n-- > 0) ++this->derived();
                return this->derived();
            }

            friend auto operator+(const derived_t& lhs, std::ptrdiff_t n) -> derived_t {
                auto copy = lhs;
                while(n-- > 0) ++copy;
                return copy;
            }

            friend auto operator+(std::ptrdiff_t n, const derived_t& rhs) -> derived_t {
                return rhs + n;
            }
        private:
            auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
        };
    }

    namespace detail {
        template <class value_t, class... args_t>
        using random_type = decltype(std::declval<value_t>() += 1);

        template <class value_t>
        const constexpr bool is_random_v = is_detected_t<random_type, value_t>::value;

        template <class derived_t, bool enabled>
        struct random_range {
            auto operator++() -> derived_t& {
                return this->derived() += 1;
            }

            auto operator--() -> derived_t& {
                return this->derived() += -1;
            }

            auto operator++(int) -> derived_t {
                auto copy = this->derived();
                return this->derived() += 1, copy;
            }

            auto operator--(int) -> derived_t {
                auto copy = this->derived();
                return this->derived() += -1, copy;
            }

            auto operator-=(std::ptrdiff_t n) -> derived_t& {
                return this->derived() += -n;
            }

            friend auto operator+(const derived_t& lhs, std::ptrdiff_t n) -> derived_t {
                return derived_t{ lhs } += n;
            }

            friend auto operator+(std::ptrdiff_t n, const derived_t& rhs) -> derived_t {
                return rhs + n;
            }

            friend auto operator-(const derived_t& lhs, std::ptrdiff_t n) -> derived_t {
                return derived_t{ lhs } -= n;
            }
        private:
            auto derived() -> derived_t& { return *static_cast<derived_t*>(this); } 
        };

        template <class derived_t>
        struct random_range<derived_t, false> {};
    }

    template <class derived_t>
    struct range_mixin :
        public detail::equal_range<derived_t, detail::is_equal_v<derived_t>>,
        public detail::forward_range<derived_t, detail::is_forward_v<derived_t>>,
        public detail::backward_range<derived_t, detail::is_backward_v<derived_t>>,
        public detail::random_range<derived_t, detail::is_random_v<derived_t>>,
        public detail::comparable_range<derived_t, detail::is_comparable_v<derived_t>>
    {};

    template <class iterator_t, class container_t>
    class iterator_wrapper :
        public iterator_t,
        public range_mixin<iterator_t> {
    public:
        template <class... args_t>
        iterator_wrapper(args_t&&... args) :
            iterator_t(std::forward<args_t>(args)...)
        {
            using namespace detail;

            // TODO: FIXME: make it work
            static_assert(has_prefix_inc_v<iterator_t> ^ has_postfix_inc_v<iterator_t> == 0,
                "Type iterator_t should have both prefix and suffix operators defined"
            );
        }
    };

    template <class iterator_t>
    class range : public range_mixin<range<iterator_t>> {
    public:
        range(iterator_t start, iterator_t finish) :
            m_it(start), m_finish(finish)
        {}
        auto operator++() -> range& { return ++m_it; }
        auto operator--() -> range& { return --m_it; }
        auto operator++(int) -> range { auto it = *this; ++m_it; return it; }
        auto operator--(int) -> range { auto it = *this; --m_it; return it; }
        explicit operator bool() const { return m_it != m_finish; }
    private:
    private:
        mutable iterator_t m_it, m_finish;
    };
}
