#pragma once

#define CONCAT(x,y) _CONCAT(x,y)
#define _CONCAT(x,y) x ## y

#define STRINGIFY(x) _STRINGIFY(x)
#define _STRINGIFY(x) #x
