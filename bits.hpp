#pragma once
#include <ftl/branchless.hpp>
#include <ftl/test.hpp>
#include <climits>
#include <cstdint>
#include <cstddef>
#include <type_traits>

namespace ftl {
    template <class value_t>
    auto rotl ( value_t x, int8_t r ) -> value_t
    {
        return (x << r) | (x >> (sizeof(value_t) * CHAR_BIT - r));
    }

    template <class value_t>
    auto rotr ( value_t x, int8_t r ) -> value_t
    {
        return (x >> r) | (x << (sizeof(value_t) * CHAR_BIT - r));
    }

    template <class pointer_t>
    auto add(void* base, std::ptrdiff_t offset) {
        auto result = reinterpret_cast<char*>(base) + offset;
        return reinterpret_cast<pointer_t>(result);
    }

    template <class value_t>
    constexpr bool is_aligned_to(const value_t value, const std::size_t base, std::false_type) {
        return !(base & (base - 1)) && !(value & (base - 1));
    }

    template <class value_t>
    constexpr bool is_aligned_to(const value_t value, const std::size_t base, std::true_type) {
        return reinterpret_cast<value_t>(is_aligned_to(reinterpret_cast<uintptr_t>(value), base, std::false_type()));
    }

    template <class value_t>
    constexpr bool is_aligned_to(const value_t value, const std::size_t base) {
        return is_aligned_to(value, base, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto align_down(const value_t value, const std::size_t base, std::false_type) -> value_t {
        return value  & ~(base - 1);
    }

    template <class value_t>
    constexpr auto align_down(const value_t value, const std::size_t base, std::true_type) -> value_t {
        return reinterpret_cast<value_t>(align_down(reinterpret_cast<std::uintptr_t>(value), base, std::false_type()));
    }

    template <class value_t>
    constexpr auto align_down(const value_t value, const std::size_t base) -> value_t {
        return align_down(value, base, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto align_up(const value_t value, const std::size_t base, std::false_type) -> value_t {
        return align_down(value + (base - 1), base);
    }

    template <class value_t>
    constexpr auto align_up(const value_t value, const std::size_t base, std::true_type) -> value_t {
        return reinterpret_cast<value_t>(align_up(reinterpret_cast<uintptr_t>(value), base, std::false_type()));
    }

    template <class value_t>
    constexpr auto align_up(const value_t value, const std::size_t base) -> value_t {
        return align_up(value, base, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto generate_mask(const std::size_t size) -> value_t {
        return (size) ? static_cast<typename std::make_unsigned<value_t>::type>(-1) >> (8 * sizeof(value_t) - size) : 0;
    }

    template <class value_t>
    constexpr auto popcnt_mask(const std::size_t patch_size, const std::size_t i = 0) -> value_t {
        static_assert(std::is_unsigned_v<value_t>, "Value type must be unsigned");
        return i < sizeof(value_t)*4 / patch_size ?
                    (generate_mask<value_t>(patch_size) << 2 * patch_size * i) | popcnt_mask<value_t>(patch_size, i + 1) :
                    0;
    }

    namespace {
        constexpr auto popcnt(std::uint64_t value) {
            constexpr auto mask1 = popcnt_mask<std::uint64_t>(1);
            value = ((value >> 1) & mask1) + (value & mask1);
            constexpr auto mask2 = popcnt_mask<std::uint64_t>(2);
            value = ((value >> 2) & mask2) + (value & mask2);
            constexpr auto mask3 = popcnt_mask<std::uint64_t>(4);
            value = ((value >> 4) & mask3) + (value & mask3);
            constexpr auto mask4 = popcnt_mask<std::uint64_t>(8);
            value = ((value >> 8) & mask4) + (value & mask4);
            constexpr auto mask5 = popcnt_mask<std::uint64_t>(16);
            value = ((value >> 16) & mask5) + (value & mask5);
            constexpr auto mask6 = popcnt_mask<std::uint64_t>(32);
            value = ((value >> 32) & mask6) + (value & mask6);
            return value;
        }

        constexpr auto popcnt(std::uint32_t value) {
            constexpr auto mask1 = popcnt_mask<std::uint32_t>(1);
            value = ((value >> 1) & mask1) + (value & mask1);
            constexpr auto mask2 = popcnt_mask<std::uint32_t>(2);
            value = ((value >> 2) & mask2) + (value & mask2);
            constexpr auto mask3 = popcnt_mask<std::uint32_t>(4);
            value = ((value >> 4) & mask3) + (value & mask3);
            constexpr auto mask4 = popcnt_mask<std::uint32_t>(8);
            value = ((value >> 8) & mask4) + (value & mask4);
            constexpr auto mask5 = popcnt_mask<std::uint32_t>(16);
            value = ((value >> 16) & mask5) + (value & mask5);
            return value;
        }

        constexpr auto popcnt(std::uint16_t value) {
            constexpr auto mask1 = popcnt_mask<std::uint16_t>(1);
            value = ((value >> 1) & mask1) + (value & mask1);
            constexpr auto mask2 = popcnt_mask<std::uint16_t>(2);
            value = ((value >> 2) & mask2) + (value & mask2);
            constexpr auto mask3 = popcnt_mask<std::uint16_t>(4);
            value = ((value >> 4) & mask3) + (value & mask3);
            constexpr auto mask4 = popcnt_mask<std::uint16_t>(8);
            value = ((value >> 8) & mask4) + (value & mask4);
            return value;
        }

        constexpr auto popcnt(std::uint8_t value) {
            constexpr auto mask1 = popcnt_mask<std::uint8_t>(1);
            value = ((value >> 1) & mask1) + (value & mask1);
            constexpr auto mask2 = popcnt_mask<std::uint8_t>(2);
            value = ((value >> 2) & mask2) + (value & mask2);
            constexpr auto mask3 = popcnt_mask<std::uint8_t>(4);
            value = ((value >> 4) & mask3) + (value & mask3);
            return value;
        }
    }

    template <class value_t>
    constexpr auto bmin(value_t lhs, value_t rhs, std::false_type) -> value_t {
        using mask_t = typename std::make_unsigned<value_t>::type;
        mask_t lhs_mask = (mask_t)bgreater(lhs, rhs) - 1;
        mask_t rhs_mask = ~lhs_mask;
        return (value_t)((lhs & lhs_mask) |
                    (rhs & rhs_mask));
    }

    template <class value_t>
    constexpr auto bmin(value_t lhs, value_t rhs, std::true_type) -> value_t {
        return reinterpret_cast<value_t>(bmin(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bmin(value_t lhs, value_t rhs) {
        return bmin((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto bmax(value_t lhs, value_t rhs, std::false_type) {
        using mask_t = typename std::make_unsigned<value_t>::type;
        mask_t lhs_mask = (mask_t)bless(lhs, rhs) - 1;
        mask_t rhs_mask = ~lhs_mask;
        return (value_t)((lhs & lhs_mask) |
                    (rhs & rhs_mask));
    }

    template <class value_t>
    constexpr auto bmax(value_t lhs, value_t rhs, std::true_type) {
        return reinterpret_cast<value_t>(bmax(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bmax(value_t lhs, value_t rhs) {
        return bmax((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto const_log2(const value_t value) -> value_t {
        return (value > 1) ? 1 + const_log2(value >> 1) : 0;
    }

    template <class value_t>
    constexpr auto clz(const value_t value) {
        using mask_t = typename std::make_unsigned<value_t>::type;
        mask_t tmp = value;
        auto count = 0;
        constexpr auto size = sizeof(mask_t) << 3;
        constexpr auto steps = const_log2(size);
        for (auto i = 0u; i < steps; ++i) {
            auto shift = size >> (i + 1);
            mask_t inv_step = (-(tmp >> shift) >> shift) & shift;
            mask_t step = shift - inv_step;
            count += step;
            tmp >>= inv_step;
        }
        return count;
    }

    template <class value_t>
    constexpr auto ctz(const value_t value) {
        using mask_t = typename std::make_unsigned<value_t>::type;
        mask_t tmp = value;
        auto count = 0;
        constexpr auto size = sizeof(mask_t) << 3;
        constexpr auto steps = const_log2(size);
        for (auto i = 0u; i < steps; ++i) {
            auto shift = size >> (i + 1);
            mask_t mask = generate_mask<mask_t>(shift);
            mask_t inv_step = (-(tmp & mask) >> shift) & shift;
            mask_t step = shift - inv_step;
            count += step;
            tmp >>= step;
        }
        return count;
    }

    template <class value_t>
    constexpr auto log2(const value_t value) {
        return sizeof(value_t) * CHAR_BIT - 1 - clz(value);
    }

    template <class value_t>
    constexpr auto div3(const value_t value) {
        std::size_t result = 0; /* the quotient */
        result = ((value >>  2) + value) >> 1; /* result = value*0.101 */
        result = ((result >>  4) + result)     ; /* result = value*0.1010101 */
        result = (result >>  9) + (result >> 1); /* result = value*0.0101010101010101 */
            /* either result = value/3 or result+1 = value/3 for all value < 12,723 */
        return result;
    };

    template <class value_t>
    constexpr auto div10(const value_t value) {
        std::size_t result = 0; /* the quotient */
        result = (value >> 2) + (value >> 1); /* result = value*0.11 */
        result = ((result >> 4) + result)     ; /* result = value*0.110011 */
        result = (result >> 8) + (result >> 3); /* result = value*0.00011001100110011 */
            /* either Q = A/10 or Q+1 = A/10 for all A < 534,890 */
        return result;
    };

    namespace detail {
        template <class value_t, class T = void> struct pow10_lut;

        template <class T>
        struct pow10_lut<std::uint8_t, T> {
            constexpr static const std::uint8_t value[] = {
                1u, 10u, 100u
            };
        };

        template <class T>
        struct pow10_lut<std::uint16_t, T> {
            constexpr static const std::uint16_t value[] = {
                1u, 10u, 100u, 1000u, 10000u
            };
        };

        template <class T>
        struct pow10_lut<std::uint32_t, T> {
            constexpr static const std::uint32_t value[] = {
                1u, 10u, 100u, 1000u, 10000u, 100000u, 
                1000000u, 10000000u, 100000000u, 1000000000u,
            };
        };

        template <class T>
        struct pow10_lut<std::uint64_t, T> {
            constexpr static const std::uint64_t value[] = {
                1u, 10u, 100u, 1000u, 10000u, 100000u, 
                1000000u, 10000000u, 100000000u, 1000000000u,
                10000000000u, 100000000000u, 1000000000000u, 10000000000000u,
                100000000000000u, 1000000000000000u, 10000000000000000u, 100000000000000000u,
                1000000000000000000u, 10000000000000000000u
            };
        };

        template <class T>
        constexpr const std::uint8_t pow10_lut<std::uint8_t, T>::value[];
        template <class T>
        constexpr const std::uint16_t pow10_lut<std::uint16_t, T>::value[];
        template <class T>
        constexpr const std::uint32_t pow10_lut<std::uint32_t, T>::value[];
        template <class T>
        constexpr const std::uint64_t pow10_lut<std::uint64_t, T>::value[];
    }

    template <class value_t>
    constexpr auto pow10(const std::size_t power) {
        using unsigned_t = typename std::make_unsigned<value_t>::type;
        return detail::pow10_lut<unsigned_t>::value[power];
    }

    template <class value_t>
    constexpr auto log10(const value_t value) {
        using unsigned_t = typename std::make_unsigned<value_t>::type;
        auto base = log2(value);
        auto row = base / 10;
        base -= row * 10u;
        auto power = row * 3;
        auto increment = (base - 4u) / 3 + 1u;
        power += select(bless((unsigned long)base, 4ul), 0ul, (unsigned long)increment); // TODO: refactoring
        return power + bgreater_or_equal((unsigned_t)value, pow10<unsigned_t>(power + 1));
    }
}

TEST
{
    using namespace ftl;
    REQUIRE(is_aligned_to(64ul, 64) == true);
    REQUIRE(is_aligned_to(65ul, 64) == false);
    REQUIRE(align_down(64ul, 64) == 64);
    REQUIRE(align_down(65ul, 64) == 64);
    REQUIRE(align_up(128ul, 64) == 128);
    REQUIRE(align_up(65ul, 64) == 128);
    REQUIRE(is_aligned_to((void*)64ul, 64) == true);
    REQUIRE(is_aligned_to((void*)65ul, 64) == false);
    REQUIRE(align_down((void*)64ul, 64) == (void*)64);
    REQUIRE(align_down((void*)65ul, 64) == (void*)64);
    REQUIRE(align_up((void*)128ul, 64) == (void*)128);
    REQUIRE(align_up((void*)65ul, 64) == (void*)128);

    REQUIRE(popcnt(std::uint64_t(0xF)) == 4);
    REQUIRE(popcnt(std::uint32_t(0xF)) == 4);
    REQUIRE(popcnt(std::uint16_t(0xF)) == 4);
    REQUIRE(popcnt(std::uint8_t(0xF)) == 4);
    REQUIRE(ctz(2ul) == 1);
    REQUIRE(clz(1ul) == sizeof(unsigned long) * CHAR_BIT - 1);
    REQUIRE(log2(0xFul) == 3);
    REQUIRE(div3(4) == 1);
    REQUIRE(div10(62) == 6);
    REQUIRE(bmax(60, 6) == 60);
    REQUIRE(bmin(60, 6) == 6);
    REQUIRE(generate_mask<unsigned>(4) == 0xF);
    REQUIRE(pow10<unsigned>(4) == 10000);
    REQUIRE(log10(10000) == 4);
}
