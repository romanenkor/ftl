#pragma once
#include <ftl/bits.hpp>
#include <type_traits>

namespace ftl {
    static constexpr auto max_prime(unsigned long long) { return 0xFFFFFFFFFFFFFFC5; }
    static constexpr auto max_prime(unsigned long) { return 0xFFFFFFFB; }
    static constexpr auto max_prime(unsigned short) { return 0xFFF1; }
    static constexpr auto max_prime(unsigned char) { return 0xFB; }

    template <class value_t>
    auto qrand(value_t value) {
        auto prime = max_prime(value);
        auto quadratic = ((unsigned long long)value * value);
        auto residue = select(greater_or_equal(quadratic, prime), quadratic - prime, quadratic);
        auto result = select(greater_or_equal(residue, prime >> 1), residue - prime, residue);
        return (value_t)result;
    }
}
