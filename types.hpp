#pragma once
#include <cstdint>

    using u64 = std::uint64_t;
    using u32 = std::uint32_t;
    using u16 = std::uint16_t;
    using u8 = std::uint8_t;

namespace ftl {
    template<unsigned...> struct index_sequence_t {};
    template<unsigned N, unsigned... S> struct make_index_sequence_t : make_index_sequence_t<N-1, N-1, S...> {};
    template<unsigned... S> struct make_index_sequence_t<0, S...>{ typedef index_sequence_t<S...> type; };

    template <class tag_t, typename value_t>
    struct alias_t : value_t {
        template <class... args_t>
        alias_t(args_t&&... args) : value_t{args...} {}
    };

    template <class T>
    class static_ {
    public:
        static auto get() -> T& { return data; }
    private:
        static T data;
    };

    template <class T>
    T static_<T>::data;

    template <class T>
    class thread_ {
    public:
        static auto get() -> T& { return data; }
    private:
        static thread_local T data;
    };

    template <class T>
    thread_local T thread_<T>::data;

    template <class T>
    struct type_c {
        using type = T;
    };

    template <class T>
    type_c<T> type;

    struct none_t {};
}
