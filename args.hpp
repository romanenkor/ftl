#pragma once
#include <string_view>
#include <sstream>
#include <unordered_map>
#include <vector>
#include "types.hpp"

namespace ftl {
    enum class option_type {
        text, number
    };

    struct option_storage {
        option_type type;
        union {
             const char** text;
             int* number;
        };
    };

    struct args_state {
        std::vector<std::string_view> args;
        std::vector<std::string_view> long_options;
        std::vector<std::string> option_errors;
        std::unordered_map<std::string_view, option_storage> options;
    };

    namespace {
        auto& args() {
            return static_<args_state>::get().args;
        }

        auto& option_errors() {
            return static_<args_state>::get().option_errors;
        }

        void option_error(std::string_view message, std::string_view option) {
            auto& args = static_<args_state>::get().args;
            auto& long_options = static_<args_state>::get().long_options;
            auto& option_errors = static_<args_state>::get().option_errors;
            auto& options = static_<args_state>::get().options;
            std::string result = std::string{ message };
            result += " -- '";
            result += option;
            result += "'";
            option_errors.push_back(std::move(result));
        }

        void option(std::string_view short_string, std::string_view long_string, const char*& text) {
            auto& args = static_<args_state>::get().args;
            auto& long_options = static_<args_state>::get().long_options;
            auto& option_errors = static_<args_state>::get().option_errors;
            auto& options = static_<args_state>::get().options;
            auto& storage = options.emplace(long_string, option_storage{}).first->second;
            long_options.reserve(128);
            long_options[short_string[1]] = long_string;
            storage.type = option_type::text;
            storage.text = &text;
        }

        void option(std::string_view short_string, std::string_view long_string, int& number) {
            auto& args = static_<args_state>::get().args;
            auto& long_options = static_<args_state>::get().long_options;
            auto& option_errors = static_<args_state>::get().option_errors;
            auto& options = static_<args_state>::get().options;
            auto& storage = options.emplace(long_string, option_storage{}).first->second;
            long_options.reserve(128);
            long_options[short_string[1]] = long_string;
            storage.type = option_type::number;
            storage.number = &number;
        }


        void parse(int argc, const char* argv[]) {
            auto& args = static_<args_state>::get().args;
            auto& long_options = static_<args_state>::get().long_options;
            auto& option_errors = static_<args_state>::get().option_errors;
            auto& options = static_<args_state>::get().options;
            args.reserve(argc);
            for (auto i = 0; i < argc;) {
                auto curr = std::string_view{ argv[i++] };
                auto next = (i < argc) ? std::string_view{ argv[i] }
                                       : std::string_view{ "" };
                auto arg = curr;

                if (curr == "-" || curr[0] != '-') {
                    args.push_back(curr);
                    continue;
                }

                if (curr == "--") {
                    for (; i < argc; ++i) {
                        args.push_back(argv[i]);
                    }
                    goto done;
                }

                bool is_short = (curr[1] != '-');
                bool is_attached = (is_short && curr[2] != '\0');
                bool has_value = (is_attached || i < argc);
                arg = (is_short) ? long_options[curr[1]] : arg;
                next = (is_attached) ? curr.substr(2) : next;
                i += !is_attached;

                if (options.end() == options.find(arg)) {
                    option_error("invalid option", curr);
                    continue;
                }

                if (!has_value) {
                    option_error("option requires an argument", curr);
                    continue;
                }

                auto& storage = (*options.find(arg)).second;
                if (storage.type == option_type::text) {
                    *storage.text = next.data();
                }
                if (storage.type == option_type::number) {
                    auto number = 0;
                    std::stringstream(next.data()) >> number;
                    *storage.number = number;
                }
            }
        done:
            args.clear();
            long_options.clear();
            option_errors.clear();
            options.clear();
        }
    }
}
