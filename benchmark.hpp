#pragma once
#include <chrono>

namespace ftl {
    template <class duration>
    struct time_sample {
        using time_point = std::chrono::steady_clock::time_point;

        time_sample(duration& time) :
            accumulator(time) {
            start = std::chrono::steady_clock::now();
        }

        ~time_sample() {
            finish = std::chrono::steady_clock::now();
            accumulator = std::chrono::duration_cast<duration>(finish - start);
        }
    private:
        duration& accumulator;
        time_point start, finish;
    };
}
