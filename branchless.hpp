#pragma once
#include <ftl/test.hpp>
#include <cassert>
#include <cstdint>
#include <type_traits>

namespace ftl {
    template <class value_t>
    constexpr auto is_add_overflow(value_t lhs, value_t rhs) -> bool {
        typename std::make_unsigned<value_t>::type mask = ((lhs & rhs) | ((lhs | rhs) & ~(lhs + rhs)));
        auto size = sizeof(mask) * 8;
        mask >>= size - 1;
        return (bool)mask;
    }

    template <class value_t>
    constexpr auto is_sub_overflow(value_t lhs, value_t rhs) -> bool {
        typename std::make_unsigned<value_t>::type mask = ((~lhs & rhs) | ((~lhs | rhs) & (lhs - rhs)));
        auto size = sizeof(mask) * 8;
        mask >>= size - 1;
        return (bool)mask;
    }

    template <class value_t>
    constexpr auto select(bool predicate, value_t true_value, value_t false_value, std::false_type) -> value_t {
        assert(predicate >> 1 == 0 && "Predicate should be bequal to 0 or 1");
        value_t false_mask = (value_t)(predicate) - 1;
        value_t true_mask = (value_t)(~false_mask);
        return (true_value & true_mask) |
                (false_value & false_mask);
    }

    template <class value_t>
    constexpr auto select(bool predicate, value_t true_value, value_t false_value, std::true_type) -> value_t {
        return reinterpret_cast<value_t>(select(predicate, reinterpret_cast<uintptr_t>(true_value), reinterpret_cast<uintptr_t>(false_value), std::false_type()));
    }

    template <class value_t>
    constexpr auto select(bool predicate, value_t true_value, value_t false_value) -> value_t {
        return select(predicate, (std::decay_t<value_t>)true_value, (std::decay_t<value_t>)false_value, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto bgreater(value_t lhs, value_t rhs, std::false_type) {
        return is_sub_overflow(rhs, lhs);
    }

    template <class value_t>
    constexpr auto bgreater(value_t lhs, value_t rhs, std::true_type) {
        return (bool)(bgreater(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bgreater(value_t lhs, value_t rhs) {
        return bgreater((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto bless(value_t lhs, value_t rhs, std::false_type) {
        return bgreater(rhs, lhs, std::false_type());
    }

    template <class value_t>
    constexpr auto bless(value_t lhs, value_t rhs, std::true_type) {
        return (bool)(bless(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bless(value_t lhs, value_t rhs) {
        return bless((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto bnot_equal(value_t lhs, value_t rhs, std::false_type) {
        typename std::make_unsigned<value_t>::type lhs_mask = (~lhs & rhs) | ((~lhs | rhs) & (lhs - rhs));
        typename std::make_unsigned<value_t>::type rhs_mask = (~rhs & lhs) | ((~rhs | lhs) & (rhs - lhs));
        typename std::make_unsigned<value_t>::type mask = lhs_mask | rhs_mask;
        auto size = sizeof(mask) * 8;
        mask >>= size - 1;
        return (bool)(mask);
    }

    template <class value_t>
    constexpr auto bnot_equal(value_t lhs, value_t rhs, std::true_type) {
        return (bool)(bnot_equal(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bnot_equal(value_t lhs, value_t rhs) {
        return bnot_equal((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto bequal(value_t lhs, value_t rhs, std::false_type) {
        return (bool)(bnot_equal(lhs, rhs, std::false_type()) ^ 1);
    }

    template <class value_t>
    constexpr auto bequal(value_t lhs, value_t rhs, std::true_type) {
        return (bool)(bequal(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bequal(value_t lhs, value_t rhs) {
        return bequal((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto bless_or_equal(value_t lhs, value_t rhs, std::false_type) {
        return (bool)(bgreater(lhs, rhs, std::false_type()) ^ 1);
    }

    template <class value_t>
    constexpr auto bless_or_equal(value_t lhs, value_t rhs, std::true_type) {
        return (bool)(bless_or_equal(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bless_or_equal(value_t lhs, value_t rhs) {
        return bless_or_equal((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }

    template <class value_t>
    constexpr auto bgreater_or_equal(value_t lhs, value_t rhs, std::false_type) {
        return (bool)(bless(lhs, rhs, std::false_type()) ^ 1);
    }

    template <class value_t>
    constexpr auto bgreater_or_equal(value_t lhs, value_t rhs, std::true_type) {
        return (bool)(bgreater_or_equal(reinterpret_cast<uintptr_t>(lhs), reinterpret_cast<uintptr_t>(rhs), std::false_type()));
    }

    template <class value_t>
    constexpr auto bgreater_or_equal(value_t lhs, value_t rhs) {
        return bgreater_or_equal((std::decay_t<value_t>)lhs, (std::decay_t<value_t>)rhs, std::is_pointer<value_t>());
    }
}

TEST
{
    using namespace ftl;
    REQUIRE(bgreater(3, 3) == false);
    REQUIRE(bgreater(4, 3) == true);
    REQUIRE(bless(3, 3) == false);
    REQUIRE(bless(2, 3) == true);
    REQUIRE(bgreater_or_equal(2, 3) == false);
    REQUIRE(bgreater_or_equal(3, 2) == true);
    REQUIRE(bgreater_or_equal(3, 3) == true);
    REQUIRE(bless_or_equal(4, 3) == false);
    REQUIRE(bless_or_equal(2, 2) == true);
    REQUIRE(bless_or_equal(2, 3) == true);
    REQUIRE(bnot_equal(3, 3) == false);
    REQUIRE(bnot_equal(4, 3) == true);
    REQUIRE(bequal(4, 3) == false);
    REQUIRE(bequal(3, 3) == true);
    REQUIRE(is_sub_overflow(4, 3) == false);
    REQUIRE(is_sub_overflow(2, 3) == true);
    REQUIRE(is_add_overflow(2, 3) == false);
    REQUIRE(is_add_overflow(0xFFFFFFFF, 0xFFFFFFFF) == true);
    REQUIRE(select(bless(1, 3), 1, 3) == 1);
    REQUIRE(select(bless(3, 3), 1, 3) == 3);
}
