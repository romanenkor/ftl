#pragma once
#include <ftl/macro.hpp>
#if defined(ENABLE_UNIT_TESTS)
#   include <ftl/types.hpp>
#   include <vector>
#   include <iostream>
#   include <type_traits>

struct check_data_t {
    const char* signature;
    const char* expression;
    bool is_executed;
    bool is_failed;
};

static std::vector<void(*)()> __tests;
static std::vector<check_data_t> __checks;

#define TEST _TEST(__COUNTER__)
#define _TEST(COUNTER) __TEST(COUNTER)
#define __TEST(COUNTER) \
    static void CONCAT(test_, COUNTER)(); \
    struct { \
        size_t value = []() { \
            return __tests.push_back(& CONCAT(test_, COUNTER)), 0; \
        }(); \
    } CONCAT(test_registration_, COUNTER); \
    static void CONCAT(test_, COUNTER)()

#define REQUIRE(expression) _REQUIRE(expression, expression)
#define _REQUIRE(expr, text) __REQUIRE(expr, #text)
#define __REQUIRE(expr, text) { \
        struct index_t { \
            size_t value = []() { \
                    auto& check = __checks.emplace_back(); \
                    check.signature = (__FILE__ ":" STRINGIFY(__LINE__) "\n"); \
                    check.expression = (text); \
                    return __checks.size() - 1; \
                }(); \
        }; \
        if (!(expr)) { \
            __checks[ftl::static_<index_t>::get().value].is_failed = true; \
        } \
        __checks[ftl::static_<index_t>::get().value].is_executed = true; \
    }

    int main(int argc, const char* argv[]) {
        (void)argc; (void)argv;
        auto failure = false;

        std::cout << "Executing " << __tests.size() << " tests\n\n";
        for (const auto test : __tests) {
            (*test)();
        }

        for (const auto check : __checks) {
            if (!check.is_executed) {
                std::cout << "Assertion \"" << check.expression
                     << "\" was not executed in:\n" "    " 
                     << check.signature;
                failure = true;
            }
            if (check.is_failed) {
                std::cout << "Assertion \"" << check.expression
                     << "\" failed in:\n" "    "
                     << check.signature;
                failure = true;
            }

        }
        std::cout << ( failure ? "\nFAILURE\n" : "\nSUCCESS\n" );
        return failure;
    }
#else
#   define TEST static void CONCAT(test_, __COUNTER__)()
#   define REQUIRE(expr)
#endif
