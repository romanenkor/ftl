# Fast Template Library (FTL) [![pipeline status](https://gitlab.com/romanenkor/ftl/badges/testing/pipeline.svg)](https://gitlab.com/romanenkor/ftl/commits/master) [![coverage report](https://gitlab.com/romanenkor/ftl/badges/testing/coverage.svg)](https://romanenkor.gitlab.io/ftl/)  

This library is still work-in-progress, no guarantees provided.

## Library list:
* `algorithm` - contains various STL-like algorithms
* `benchmark` - measure program performance
* `biginteger` - mathematics on numbers, that exceed machine word size
* `bits` - bit manipulation algorithms (popcnt, clz, ctz, etc.)
* `container` - data storage primitives
* `cpu` - access to CPU-specific functions
* `format` - convenient string formatting
* `functional` - functional programming (delegates, coroutines, etc.)
* `macro` - preprocessor functions
* `memory` - memory management primitives
* `parser` - convenient parsing of large datasets
* `random` - random sequence generators
* `string` - data structures & algorithms for string manipulation
* `test` - custom unit test library
* `types` - meta-programming utilities
* `vulkan` - convenient usage of the new graphics API
