#pragma once
#include <ftl/types.hpp>
#include <ftl/test.hpp>
#include <tuple>

namespace ftl {
    template <class F, class T, int... Indices>
    decltype(auto) _apply(F&& f, T&& args, index_sequence_t<Indices...>) {
        return f(std::get<Indices>(args)...);
    }

    template <class F, class... Args>
    decltype(auto) apply(F&& f, std::tuple<Args&&...> args) {
        return _apply(std::forward<F>(f), args, typename make_index_sequence_t<sizeof...(Args)>::type());
    }

    template <class F, class M, class T, int... Indices>
    decltype(auto) _apply(F&& object, M&& method, T&& args, index_sequence_t<Indices...>) {
        return (object.*method)(std::get<Indices>(args)...);
    }

    template <class F, class M, class... Args>
    decltype(auto) apply(F&& object, M&& method, std::tuple<Args&&...> args) {
        return _apply(std::forward<F>(object), std::forward<M>(method), args, typename make_index_sequence_t<sizeof...(Args)>::type());
    }
}
