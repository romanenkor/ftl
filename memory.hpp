#pragma once
#include <ftl/arch.hpp>
#include <ftl/bits.hpp>
#include <ftl/test.hpp>
#include <cstdint>
#include <cstddef>
#include <cstdlib>
#include <cassert>
#include <vector>

#if defined(_WIN32) || defined(__CYGWIN__)
#   include <Windows.h>
#   include <psapi.h>
#   define FTL_MEMORY_WINDOWS
#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
#   include <unistd.h>
#   include <sys/resource.h>
#   include <sys/types.h>
#   include <sys/param.h>
#   include <sys/mman.h>
#   if defined(__APPLE__) && defined(__MACH__)
#       include <mach/mach.h>
#       define FTL_MEMORY_APPLE
#   endif
#   if defined(BSD)
#       include <sys/sysctl.h>
#       define FTL_MEMORY_BSD
#   endif
#   if (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
#      include <fcntl.h>
#      include <procfs.h>
#      define FTL_MEMORY_SYSTEMV
#   elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
#      include <stdio.h>
#      define FTL_MEMORY_LINUX
#   endif
#   define FTL_MEMORY_POSIX
#endif

#if !defined(FTL_MEMORY_WINDOWS) && !defined(FTL_MEMORY_POSIX)
#   error "Unable to implement memory module for an unknown OS."
#endif

namespace ftl { namespace vm { namespace {
#if defined(FTL_MEMORY_WINDOWS)
    struct protection {
        protection() :
            m_protection(PAGE_NOACCESS)
        {}

        protection(DWORD protection) :
            m_protection(protection)
        {}

        friend auto operator|(protection lhs, protection rhs) -> protection {
            switch (lhs.m_protection | rhs.m_protection) {
            // 0x2
            case 0 | PAGE_READONLY: return { PAGE_READWRITE };
            // 0x1 .. 0x10
            case PAGE_NOACCESS | PAGE_READONLY: return { PAGE_READONLY };
            case PAGE_NOACCESS | PAGE_READWRITE: return { PAGE_READWRITE };
            case PAGE_NOACCESS | PAGE_EXECUTE: return { PAGE_EXECUTE };
            case PAGE_READWRITE | PAGE_READONLY: return { PAGE_READWRITE };
            case PAGE_EXECUTE | PAGE_READONLY: return { PAGE_EXECUTE_READ };
            case PAGE_EXECUTE | PAGE_READWRITE: return { PAGE_EXECUTE_READWRITE };

            default: assert(false && "Invalid protection modifiers");
            }
        }

        explicit operator DWORD() const { return m_protection; }
    private:
        DWORD m_protection;
    };

    const static constexpr auto no_access = protection{ PAGE_NOACCESS };
    const static constexpr auto read = protection{ PAGE_READONLY };
    const static constexpr auto write = protection{ PAGE_READWRITE };
    const static constexpr auto execute = protection{ PAGE_EXECUTE };
#else
    enum protection {
        no_access = PROT_NONE,
        read = PROT_READ,
        write = PROT_WRITE,
        execute = PROT_EXEC
    };

    inline auto operator|(protection lhs, protection rhs) {
        return static_cast<protection>(static_cast<int>(lhs) | static_cast<int>(rhs));
    }
#endif

    auto size_available() -> size_t {
    #if defined(FTL_MEMORY_WINDOWS)
        MEMORYSTATUSEX status;
        status.dwLength = sizeof(status);
        GlobalMemoryStatusEx(&status);
        return status.ullTotalPhys;

    #elif defined(FTL_MEMORY_POSIX)
        auto pages = sysconf(_SC_PHYS_PAGES);
        auto page_size = sysconf(_SC_PAGE_SIZE);
        return pages * page_size;
    #endif
    }

    auto size_physical() -> size_t {
    #if defined(FTL_MEMORY_WINDOWS) && (defined(__CYGWIN__) || defined(__CYGWIN32__))
        MEMORYSTATUS status;
        status.dwLength = sizeof(status);
        GlobalMemoryStatus( &status );
        return (size_t)status.dwTotalPhys;

    #elif defined(FTL_MEMORY_WINDOWS)
        MEMORYSTATUSEX status;
        status.dwLength = sizeof(status);
        GlobalMemoryStatusEx( &status );
        return (size_t)status.ullTotalPhys;

    #elif defined(FTL_MEMORY_POSIX)
    #   if defined(CTL_HW) && (defined(HW_MEMSIZE) || defined(HW_PHYSMEM64))
            int mib[2];
            mib[0] = CTL_HW;
    #       if defined(HW_MEMSIZE)
                mib[1] = HW_MEMSIZE;            /* OSX. --------------------- */
    #       elif defined(HW_PHYSMEM64)
                mib[1] = HW_PHYSMEM64;          /* NetBSD, OpenBSD. --------- */
    #       endif
        int64_t size = 0;               /* 64-bit */
        size_t len = sizeof( size );
        if ( sysctl( mib, 2, &size, &len, NULL, 0 ) == 0 )
            return (size_t)size;
        return 0L;			/* Failed? */

    #   elif defined(_SC_AIX_REALMEM)
            /* AIX. ----------------------------------------------------- */
            return (size_t)sysconf( _SC_AIX_REALMEM ) * (size_t)1024L;

    #   elif defined(_SC_PHYS_PAGES) && defined(_SC_PAGESIZE)
            /* FreeBSD, Linux, OpenBSD, and Solaris. -------------------- */
            return (size_t)sysconf( _SC_PHYS_PAGES ) *
                (size_t)sysconf( _SC_PAGESIZE );

    #   elif defined(_SC_PHYS_PAGES) && defined(_SC_PAGE_SIZE)
            /* Legacy. -------------------------------------------------- */
            return (size_t)sysconf( _SC_PHYS_PAGES ) *
                (size_t)sysconf( _SC_PAGE_SIZE );

    #   elif defined(CTL_HW) && (defined(HW_PHYSMEM) || defined(HW_REALMEM))
            /* DragonFly BSD, FreeBSD, NetBSD, OpenBSD, and OSX. -------- */
            int mib[2];
            mib[0] = CTL_HW;
    #       if defined(HW_REALMEM)
                mib[1] = HW_REALMEM;		/* FreeBSD. ----------------- */
    #       elif defined(HW_PYSMEM)
                mib[1] = HW_PHYSMEM;		/* Others. ------------------ */
    #       endif
            unsigned int size = 0;		/* 32-bit */
            size_t len = sizeof( size );
            if ( sysctl( mib, 2, &size, &len, NULL, 0 ) == 0 )
                return (size_t)size;
            return 0L;			/* Failed? */
    #   endif /* sysctl and sysconf variants */
    #else
        return 0L;			/* Unknown OS. */
    #endif
    }

    auto size_resident_peak() -> size_t {
    #if defined(FTL_MEMORY_SYSTEMV)
        /* AIX and Solaris ------------------------------------------ */
        struct psinfo psinfo;
        int fd = -1;
        if ( (fd = open( "/proc/self/psinfo", O_RDONLY )) == -1 )
            return (size_t)0L;		/* Can't open? */
        if ( read( fd, &psinfo, sizeof(psinfo) ) != sizeof(psinfo) )
        {
            close( fd );
            return (size_t)0L;		/* Can't read? */
        }
        close( fd );
        return (size_t)(psinfo.pr_rssize * 1024L);

    #elif defined(FTL_MEMORY_POSIX)
        /* BSD, Linux, and OSX -------------------------------------- */
        struct rusage rusage;
        getrusage( RUSAGE_SELF, &rusage );
    #   if defined(FTL_MEMORY_APPLE)
            return (size_t)rusage.ru_maxrss;
    #   else
            return (size_t)(rusage.ru_maxrss * 1024L);
    #   endif

    #else
        return (size_t)0L;			/* Unsupported. */
    #endif
    }

    auto size_resident() -> size_t {
    #if defined(FTL_MEMORY_APPLE)
        struct mach_task_basic_info info;
        mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
        if ( task_info( mach_task_self( ), MACH_TASK_BASIC_INFO,
            (task_info_t)&info, &infoCount ) != KERN_SUCCESS )
            return (size_t)0L;		/* Can't access? */
        return (size_t)info.resident_size;

    #elif defined(FTL_MEMORY_LINUX)
        long rss = 0L;
        FILE* fp = NULL;
        if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL )
            return (size_t)0L;		/* Can't open? */
        if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
        {
            fclose( fp );
            return (size_t)0L;		/* Can't read? */
        }
        fclose( fp );
        return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE);

    #else
        /* AIX, BSD, Solaris, and Unknown OS ------------------------ */
        return (size_t)0L;			/* Unsupported. */
    #endif
    }

    auto reserve(std::size_t size, void* address = nullptr) -> void* {
    #if defined(FTL_MEMORY_WINDOWS)
        return VirtualAlloc(address, size, MEM_RESERVE, PAGE_READWRITE);

    #elif defined(FTL_MEMORY_POSIX)
        return mmap(address, size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, -1, 0);
    #endif
    }

    bool release(void* address, std::size_t size) {
    #if defined(FTL_MEMORY_WINDOWS)
        return VirtualFree(address, 0, MEM_RELEASE);
        
    #elif defined(FTL_MEMORY_POSIX)
        return !munmap(address, size);
    #endif
    }

    auto remap(void* address, std::size_t new_size, std::size_t old_size) -> void* {
    #if defined(FTL_MEMORY_WINDOWS)
        auto new_memory = reserve(new_size);
        ::memcpy(new_memory, address, old_size);
        release(address, old_size);
        return new_memory;

    #elif defined(FTL_MEMORY_POSIX)
        return mremap(address, old_size, new_size, MREMAP_MAYMOVE);
    #endif
    }

    bool commit(void* address, std::size_t size) {
    #if defined(FTL_MEMORY_WINDOWS)
        return VirtualAlloc(address, size, MEM_COMMIT, PAGE_READWRITE);

    #elif defined(FTL_MEMORY_POSIX)
        return !mprotect(address, size, PROT_READ | PROT_WRITE);
    #endif
    }

    bool uncommit(void* address, std::size_t size) {
    #if defined(FTL_MEMORY_WINDOWS)
        return VirtualFree(address, size, MEM_DECOMMIT);

    #elif defined(FTL_MEMORY_POSIX)
        return !madvise(address, size, MADV_DONTNEED);
    #endif
    }

    bool protect(void* address, std::size_t size, protection protection) {
    #if defined(FTL_MEMORY_WINDOWS)
        DWORD old_protection;
        return VirtualProtect(address, size, protection, &old_protection);

    #elif defined(FTL_MEMORY_POSIX)
        return !mprotect(address, size, protection);
    #endif
    }

    bool lock(void* address, std::size_t size) {
    #if defined(FTL_MEMORY_WINDOWS)
        return VirtualLock(address, size);

    #elif defined(FTL_MEMORY_POSIX)
        return !mlock(address, size);
    #endif
    }

    bool unlock(void* address, std::size_t size) {
    #if defined(FTL_MEMORY_WINDOWS)
        return VirtualUnlock(address, size);

    #elif defined(FTL_MEMORY_POSIX)
        return !munlock(address, size);
    #endif
    }
}}}


TEST
{
    using namespace ftl;
    auto address = vm::reserve(page_size);
    REQUIRE(address != nullptr);
    REQUIRE(vm::commit(address, page_size));
    REQUIRE(vm::protect(address, page_size, vm::read | vm::write));
    REQUIRE(vm::lock(address, page_size));
    REQUIRE(vm::unlock(address, page_size));
    REQUIRE(vm::release(address, page_size));
}

