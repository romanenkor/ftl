#pragma once
#define FTL_X86

#include <cstdint>

namespace ftl {
    static constexpr auto cache_line_size = 64;
    static constexpr auto page_size = 4096;
    static constexpr auto aligment = 32;

    class context_t {
    public:
        inline void save();
        inline void save(const context_t& next);
        inline void restore();
    private:
        // EFLAGS?
        // FP registers?
        // Segment registers?
        std::uint32_t r[7];
        std::uint32_t ip;
    };

    inline void context_t::save() {
        auto end = this + 1;
        __asm__ volatile(R"(
            xchg %0, %%esp
            call _%=f
        _%=: add $0xb, (%%esp)
            pushd %%eax;
            pushd %%ecx;
            pushd %%edx;
            pushd %%ebx;
            pushd %%ebp;
            pushd %%esi;
            pushd %%edi;
            xchg %0, %%esp
        )"
        : "+r"(end), "=m"(*this)
        :: "cc"
        );
    }

    inline void context_t::save(const context_t& next) {
        auto end = this + 1;
        __asm__ volatile(R"(
            xchg %0, %%esp
            call _%=
        _%=: add $0x17, (%%esp)
            pushd %%eax;
            pushd %%ecx;
            pushd %%edx;
            pushd %%ebx;
            pushd %%ebp;
            pushd %%esi;
            pushd %%edi;
            xchg %2, %%esp
            popd %%edi
            popd %%esi
            popd %%ebp
            popd %%ebx
            popd %%edx
            popd %%ecx
            popd %%eax
            jmp *(%%esp)
            xchg %0, %%esp
        )"
        : "+r"(end), "=m"(*this)
        : "r"(&next), "m"(next)
        : "cc"
        );
    }

    inline void context_t::restore() {
        __asm__ volatile(R"(
            xchg %0, %%esp
            popd %%edi
            popd %%esi
            popd %%ebp
            popd %%ebx
            popd %%edx
            popd %%ecx
            popd %%eax
            jmp *(%%esp)
        )"
        :: "r"(this), "m"(*this)
        );
    }
}
