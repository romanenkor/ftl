#pragma once
#define FTL_X86_64
#define FTL_HUGEPAGE

#include <cstdint>

namespace ftl {
    const static constexpr auto huge_page_size = 2048 * 1024;
    const static constexpr auto cache_line_size = 64;
    const static constexpr auto page_size = 4096;
    const static constexpr auto aligment = 32;

    class context_t {
    public:
        inline void save();
        inline void save(const context_t& next);
        inline void restore();
    private:
        // EFLAGS?
        // FP registers?
        // Segment registers?
        std::uint64_t r[15];
        std::uint64_t ip;
    };

    inline void context_t::save() {
        volatile auto end = this + 1;
        __asm__ volatile(R"(
            xchg %0, %%rsp
            callq _%=
        _%=: addq $0x1c, (%%rsp)
            pushq %%r15
            pushq %%r14
            pushq %%r13
            pushq %%r12
            pushq %%r11
            pushq %%r10
            pushq %%r9
            pushq %%r8
            pushq %%rbp
            pushq %%rdi
            pushq %%rsi
            pushq %%rdx
            pushq %%rcx
            pushq %%rbx
            pushq %%rax
            xchg %0, %%rsp
        )"
        : "+r"(end), "=m"(*this)
        :: "cc"
        );
    }

    inline void context_t::save(const context_t& next) {
        volatile auto end = this + 1;
        __asm__ volatile(R"(
            xchg %0, %%rsp
            callq _%=
        _%=:addq $0x39, (%%rsp)
            pushq %%r15
            pushq %%r14
            pushq %%r13
            pushq %%r12
            pushq %%r11
            pushq %%r10
            pushq %%r9
            pushq %%r8
            pushq %%rbp
            pushq %%rdi
            pushq %%rsi
            pushq %%rdx
            pushq %%rcx
            pushq %%rbx
            pushq %%rax
            xchg %2, %%rsp
            popq %%rax
            popq %%rbx
            popq %%rcx
            popq %%rdx
            popq %%rsi
            popq %%rdi
            popq %%rbp
            popq %%r8
            popq %%r9
            popq %%r10
            popq %%r11
            popq %%r12
            popq %%r13
            popq %%r14
            popq %%r15
            jmpq *(%%rsp)
            xchg %0, %%rsp
        )"
        : "+r"(end), "=m"(*this)
        : "bcdSD"(&next), "m"(next)
        : "cc"
        );
    }

    inline void context_t::restore() {
        __asm__ volatile(R"(
            xchg %0, %%rsp
            popq %%rax
            popq %%rbx
            popq %%rcx
            popq %%rdx
            popq %%rsi
            popq %%rdi
            popq %%rbp
            popq %%r8
            popq %%r9
            popq %%r10
            popq %%r11
            popq %%r12
            popq %%r13
            popq %%r14
            popq %%r15
            jmpq *(%%rsp)
        )"
        :: "r"(this), "m"(*this)
        : "cc"
        );
    }
}
